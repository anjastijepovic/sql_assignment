CREATE DATABASE COMPANY;

CREATE TABLE COMPANY_DEPARTMENTS (
    ID_DEP int,
    NAME_DEP VARCHAR(50),
    LOCATION VARCHAR(50) DEFAULT 'Podgorica' CHECK (LOCATION IN ('Podgorica', 'Nikšić', 'Žabljak', 'Bar')),
    MANAGER VARCHAR(50),
    PRIMARY KEY (ID_DEP)
    
);

    
CREATE TABLE COMPANY_EMPLOYEES (
    ID int NOT NULL CHECK(ID between 1 and 999),
    DEP int,
    NAME_EMP VARCHAR(40),
    PHONE_NUMBER VARCHAR(12),
    DATE_OF_EMPLOYMENT DATE,
    SALARY DECIMAL(10, 2),
    PRIMARY KEY (ID),
    FOREIGN KEY (DEP) REFERENCES COMPANY_DEPARTMENTS(ID_DEP)
);

INSERT INTO COMPANY.COMPANY_DEPARTMENTS (ID_DEP, NAME_DEP, LOCATION, MANAGER)
VALUES
    (1, 'IT', 'Nikšić', 'Ivan Ivanović'),
    (2, 'HR', 'Podgorica', 'Marko Marković');
    
INSERT INTO COMPANY.COMPANY_EMPLOYEES (ID, DEP, NAME_EMP, PHONE_NUMBER, DATE_OF_EMPLOYMENT, SALARY)
VALUES
    (1, 1, 'Ivan Ivanović', '067638908', '2015-01-15', 2500),
    (2, 2, 'Marko Marković', '067897654', '2012-01-02', 2000);


