SELECT CD.MANAGER, CE.DATE_OF_EMPLOYMENT
FROM COMPANY_DEPARTMENTS CD
JOIN COMPANY_EMPLOYEES CE ON CD.MANAGER = CE.NAME_EMP
WHERE CE.PHONE_NUMBER LIKE '067%'
ORDER BY CE.DATE_OF_EMPLOYMENT ASC
LIMIT 1;



SELECT SUM(CE.SALARY) AS TOTAL_SALARY
FROM COMPANY_DEPARTMENTS CD
JOIN COMPANY_EMPLOYEES CE ON CD.ID_DEP = CE.DEP
WHERE CD.LOCATION = 'Podgorica';DROP TABLE MANAGERS;


CREATE TABLE MANAGERS (
    EMPLOYEE_ID int,
    DEP_ID int,
    PROMOTION_DATE DATE,
    DISMISSAL_DATE DATE,
    PRIMARY KEY (DEP_ID, EMPLOYEE_ID),
    FOREIGN KEY (EMPLOYEE_ID) REFERENCES COMPANY_EMPLOYEES(ID),
    FOREIGN KEY (DEP_ID) REFERENCES COMPANY_DEPARTMENTS(ID_DEP),
    CHECK (PROMOTION_DATE <= DISMISSAL_DATE OR DISMISSAL_DATE IS NULL),
    CONSTRAINT UQ_Department_Manager UNIQUE (DEP_ID, PROMOTION_DATE)
);

