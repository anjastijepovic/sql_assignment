/*
  4.The table COMPANY_EMPLOYEES has columns ID, NAME, DEPARTMENT, and DEPARTMENT_LOCATION. 
  Could it be said that this table is in the 1NF? 
  If not, what changes would be necessary to achieve the first normal form?

Every employee_ID has exactly one department, but a department can have more than one employee
so the tables employees and departments have to be separate
*/

CREATE TABLE COMPANY_DEPARTMENTS (
    ID_DEP int,
    DEP_NAME VARCHAR (50),
    DEP_LOCATION VARCHAR (50),
    PRIMARY KEY (ID_DEP)
);

    
CREATE TABLE COMPANY_EMPLOYEES (
    ID int,
    DEP int,
    PRIMARY KEY (ID),
    FOREIGN KEY (DEP) REFERENCES COMPANY_DEPARTMENTS(ID_DEP)
);
