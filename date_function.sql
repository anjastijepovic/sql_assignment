-- Create a function that returns all employees whose date_of_employment 
-- is equal to tommorow's date (dd-mm)(ignoring the year).

/*
INSERT INTO COMPANY.COMPANY_EMPLOYEES (ID, DEP, NAME_EMP, PHONE_NUMBER, DATE_OF_EMPLOYMENT, SALARY)
VALUES
    (5, 1, 'Stefan Rakočević', '063638908', '2023-01-25', 600);
*/

SELECT * FROM COMPANY_EMPLOYEES
WHERE DATE_FORMAT(DATE_OF_EMPLOYMENT, '%D %M') = DATE_FORMAT(DATE_ADD(CURDATE(), INTERVAL 1 DAY), '%D %M');
